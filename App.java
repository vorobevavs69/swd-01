package ru.vorobieva.selenium;

import java.util.Scanner;


public class App {
    public static void main(String[] args) {

        System.out.println("** CALCULATOR **");
        final Scanner scanner = new Scanner(System.in);
        System.out.println("ENTER A:");
        final int a = scanner.nextInt();
        System.out.println("ENTER B:");
        final int b = scanner.nextInt();
        final Calculator calculator = new Calculator();
        System.out.println(calculator.sum(a,b));
    }
}
